from starlette.applications import Starlette
from starlette.responses import HTMLResponse, JSONResponse
from starlette.staticfiles import StaticFiles
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import StreamingResponse

from fastai import *
from fastai.vision import *
from fastai.callbacks import *
from PIL import Image

import uvicorn, aiohttp, asyncio
import os
import io
import requests
import pandas as pd
import psycopg2 as psy

defaults.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')


#export_file_url = 'https://drive.google.com/uc?export=download&id=1w5iRzJyYal2v8VZojwjHubJY7hT9MRTk'

#export_file_name = 'export.pkl'

export_file_bin = 'https://drive.google.com/uc?export=download&id=1WOd8-xgzxOKhqoDpqJKleneCpLS31C3O'

export_file_bin_name = 'bug_analyzer_bin.pkl'

export_file_classes = 'https://drive.google.com/uc?export=download&id=10UXtQ18BlURAW4DN20ZadjoD-abbWQRS'

export_file_classes_name = 'bug_model_classes_spiders1_32.pkl'


path = Path(__file__).parent

app = Starlette()
app.add_middleware(CORSMiddleware, allow_origins=['*'], allow_headers=['X-Requested-With', 'Content-Type'])
app.mount('/static', StaticFiles(directory='app/static'))

async def download_file(url, dest):
    if dest.exists(): return
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            data = await response.read()
            with open(dest, 'wb') as f: f.write(data)

async def setup_learner():
    
    await download_file(export_file_bin, path/export_file_bin_name)
    await download_file(export_file_classes, path/export_file_classes_name)
    
    try:
        learn_bin = load_learner(path, export_file_bin_name)
        learn_classes = load_learner(path, export_file_classes_name)
        return learn_bin, learn_classes
    except RuntimeError as e:
        if len(e.args) > 0 and 'CPU-only machine' in e.args[0]:
            print(e)
            message = "\n\nThis model was trained with an old version of fastai and will not work in a CPU environment.\n\nPlease update the fastai library in your training environment and export your model again.\n\nSee instructions for 'Returning to work' at https://course.fast.ai."
            raise RuntimeError(message)
        else:
            raise

loop = asyncio.get_event_loop()
tasks = [asyncio.ensure_future(setup_learner())]
learn_bin, learn_classes = loop.run_until_complete(asyncio.gather(*tasks))[0]
loop.close()

@app.route('/')
def index(request):
    html = path/'view'/'index.html'
    return HTMLResponse(html.open().read())

@app.route('/bug_bin', methods=['POST'])
async def bug_bin(request):
    data = await request.form()
    img_bytes = await (data['file'].read())
    img = open_image(BytesIO(img_bytes))
    prediction, _, probs = learn_bin.predict(img)
    to_float3 = lambda x: format((x*100), '.3f')
    probs = list(map(to_float3, probs.tolist()))
    
    all_classes = ['accessory','animal','ant','appliance','assassin','bed-bug','bee','beetle','butterfly',
    'cicadomorpha-bug','cockroach','crickets', 'electronic','flies','food','furniture','indoor',
    'kitchen','lacewing','leaf-footed-squash-bug','outdoor','person','plant-bug','planthopper-bug',
    'seed-bug','sports','stink-shield-bug','termites','thrips','vehicle','wasp']
    
    all_insect = ['ant','assassin','bed-bug','bee','beetle','butterfly','catterpillar',\
    'cicadomorpha-bug','cockroach','crickets','dragonfly','flies','lacewing',\
    'leaf-footed-squash-bug','plant-bug','planthopper-bug','seed-bug',\
    'stink-shield-bug']
    

    if str(prediction) in all_insect:
        return JSONResponse({'result': f'É um inseto {list(zip(all_classes, probs))}'})
    else:
        return JSONResponse({'result': f'Não é inseto {list(zip(all_classes, probs))}'})
        
        
        

@app.route('/bug_analyze', methods=['POST'])
async def bug_analyze(request):
    
    
    # PREDICT MODEL
    ########################
    img = await request.form()
    
    img_url = img["imagem"]
    
    response = requests.get(img_url)
    
    img_bytes = response.content
    
    img = open_image(BytesIO(img_bytes))

    probs = learn_bin.predict(img)[2]
    
    
    
    # NON INSECT
    ########################
    def isInsect(probs,thresh = 40):
    
        to_float3 = lambda x: format((x*100), '.3f')
        
        probs = list(map(to_float3, probs.tolist()))
        
        insect = ['ant','assassin','bed-bug','bee','beetle','butterfly','catterpilar','cicadomorpha-bug','cockroach',
              'crickets','dragonfly','flies','lacewing','leaf-footed-squash-bug','plant-bug','planthopper-bug',
              'seed-bug','stink-shield-bug','termites','thrips','wasp']
        
        probs_df = pd.DataFrame({'Classe':learn_bin.data.classes, 'Probabilidade':probs}, 
                                columns=['Classe','Probabilidade'])
        
        insect_prob = probs_df[probs_df['Classe'].isin(insect)].Probabilidade.astype(float).sum()
        
        return (100 - insect_prob) <= (100 - thresh)
    
    json_noninsect = {'inseto':'false', 'filo' :'', 'classe':'', 'ordem':'', 'nome':'', 'confidence': 0}
    
    if not isInsect(probs):
        return JSONResponse(json_noninsect)
        
    
    
    # INSECT
    ########################
    prediction_class, _, probs = learn_classes.predict(img)
    
    probmax = max(probs.tolist())
    
    confidence = str(round(probmax*100,1))
    
    json_insect = \
                    [
                        {'inseto':'true', 'filo' :'Arthropoda', 'classe':'Insecta', 'ordem':'Hymenoptera', 'nome':'Formiga', 'confidence': confidence},
                        {'inseto':'true', 'filo' :'Arthropoda', 'classe':'Insecta', 'ordem':'Hymenoptera', 'nome':'Abelha', 'confidence': confidence},
                        {'inseto':'true', 'filo' :'Arthropoda', 'classe':'Insecta', 'ordem':'Coleoptera', 'nome':'Besouro', 'confidence': confidence},
                        {'inseto':'true', 'filo' :'Arthropoda', 'classe':'Insecta', 'ordem':'Lepidoptera', 'nome':'Borboleta', 'confidence': confidence},
                        {'inseto':'true', 'filo' :'Arthropoda', 'classe':'Insecta', 'ordem':'Blattodea', 'nome':'Barata', 'confidence': confidence},
                        {'inseto':'true', 'filo' :'Arthropoda', 'classe':'Insecta', 'ordem':'Odonata', 'nome':'Libélula', 'confidence': confidence},
                        {'inseto':'true', 'filo' :'Arthropoda', 'classe':'Insecta', 'ordem':'Diptera', 'nome':'Mosca', 'confidence': confidence},
                        {'inseto':'false', 'filo' :'Arthropoda', 'classe':'Chelicerata', 'ordem' :'Aracnae', 'nome':'Aranha', 'confidence': confidence}
                    ]
                
    
    all_insect_classes = ['ant', 'bee', 'beetle', 'butterfly', 'cockroach', 'dragonfly', 'flies', 'spider']
    
    try:
        pos = all_insect_classes.index(str(prediction_class))
        
    except ValueError as e:
        return f"Houve um erro: {e}"
    
    
    # CALL POSTGRESQL
    ########################
    try:
        connection = psy.connect(user="bugslifedb_user", password="QZoYR4LNKkTcIIYOefxCCjf37sUqR1RN", 
                                      host = "postgres.render.com", port="5432", database="bugslifedb")
        cursor = connection.cursor()
        query = """INSERT INTO bugs_images(image, prediction) VALUES (%s,%s)"""
                    
        binary = psy.Binary(img_bytes)
        
        data = (binary, pos)
        
        cursor.execute(query, data)
        
        connection.commit()
        
        if(connection):
            cursor.close()
            connection.close()
    
    except (Exception, psy.Error) as error :
        pass
        #return JSONResponse({"status":f"Error while executing PostgreSQL: {error}"})
        
    
    try:
        pos = all_insect_classes.index(str(prediction_class))
        return JSONResponse(json_insect[pos])
        
    except ValueError as e:
        return f"Houve um erro: {e}"
        


@app.route('/err_pred', methods=['POST'])
async def bug_analyze(request):
    
    
    # GET IMAGE
    ########################
    img = await request.form()
    
    img_url = img["imagem"]
    prediction = img["nome"]
    
    response = requests.get(img_url)
    
    img_bytes = response.content
    
    all_insect_classes = ['Formiga', 'Abelha', 'Besouro', 'Borboleta', 'Barata', 'Libélula', 'Mosca', 'Aranha']
    
    try:
        prediction = all_insect_classes.index(prediction)
    
    except ValueError as e:
        return JSONResponse({"status":"OK"})
    
    
    # CALL POSTGRESQL
    ########################
    try:
        connection = psy.connect(user="bugslifedb_user", password="QZoYR4LNKkTcIIYOefxCCjf37sUqR1RN", 
                                      host = "postgres.render.com", port="5432", database="bugslifedb")
        cursor = connection.cursor()
        query = """INSERT INTO bugs_images(image, prediction) VALUES (%s,%s)"""
                    
        binary = psy.Binary(img_bytes)
        
        data = (binary, prediction)
        
        cursor.execute(query,data)
        
        connection.commit()
        
        if(connection):
            cursor.close()
            connection.close()
            
        return JSONResponse({"status":"OK"})
    
    except (Exception, psy.Error) as error :
        return JSONResponse({"status":f"Error while executing PostgreSQL: {error}"})



@app.route('/t_learn')
async def t_learn(request):
    
    
    _id = request.query_params['id']

    
    try:
        connection = psy.connect(user="bugslifedb_user", password="QZoYR4LNKkTcIIYOefxCCjf37sUqR1RN", 
                                      host = "postgres.render.com", port="5432", database="bugslifedb")
        cursor = connection.cursor()
        
        query = f"""SELECT * FROM bugs_images WHERE id = {_id}"""
        
        cursor.execute(query)
    
    except (Exception, psy.Error) as error :
        return JSONResponse({"status":f"Error while executing PostgreSQL: {error}"})
    
    
    img = cursor.fetchone()[0]
    
    #stream = BytesIO(img)
    
    #image = Image.open(stream)
    
    #image.save('img.jpg', format = 'JPEG')
    
    #stream.seek(0)
    
    
    generator = image_res(img)
        
    return StreamingResponse(generator, media_type="image/jpeg")
    

    # return JSONResponse({"status":"OK"})
    
    # imsave(buf, img, format='JPEG', quality=100)
    
    # buf = BytesIO()
    # imsave(buf, img, format='JPEG', quality=100)
    # buf.seek(0)
    
    
async def image_res(img):
    
    stream = io.BytesIO(img)
    
    image = Image.open(stream)
    
    imgByteArr = io.BytesIO()
    image.save(imgByteArr, format='JPEG')

    imgByteArr = imgByteArr.getvalue()

    
    yield(imgByteArr)
     

    
if __name__ == '__main__':
    if 'serve' in sys.argv:
        port = int(os.getenv('PORT', 5042))
        uvicorn.run(app=app, host='0.0.0.0', port=port)
